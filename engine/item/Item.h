#ifndef ENGINE_ITEM_H
#define ENGINE_ITEM_H


class Item {
private:
    int x;
    int y;
    char symbol;
    int bombsRadiusModifier;
    int bombsDelayModifier;
    int bombsCountModifier;
    int actionsCountModifier;
    int playerHealthModifier;
    bool canTriggerBombsModifier;
    int playerInvulnerableModifier;

public:
    Item(char symbol, int radius, int delay, int bombsCount, int actionsCount, int playerHealthModifier, bool canTriggerBombs, int playerInvulnerableModifier) : symbol(symbol), bombsRadiusModifier(radius), bombsDelayModifier(delay), bombsCountModifier(bombsCount), actionsCountModifier(actionsCount), playerHealthModifier(playerHealthModifier), canTriggerBombsModifier(canTriggerBombs), playerInvulnerableModifier(playerInvulnerableModifier) {}
    int getX() const;
    int getY() const;
    char getSymbol() const;
    int getBombsRadiusModifier() const;
    int getBombsDelayModifier() const;
    int getBombsCountModifier() const;
    int getActionsCountModifier() const;
    int getPlayerHealthModifier() const;
    bool isCanTriggerBombsModifier() const;
    int getPlayerInvulnerableModifier() const;
    void setX(int x);
    void setY(int y);
};


#endif

#ifndef ENGINE_PLAYER_H
#define ENGINE_PLAYER_H


#include <vector>
#include "../item/Item.h"

class Player {
private:
    int id;
    int x;
    int y;
    int health;
    int bombsCount;
    int bombsRadius;
    int bombsDelay;
    int actionsPerTurn;
    bool canTriggerBombs;
    int invulnerableTurns;

public:
    explicit Player(int id, int health, int bombsCount, int bombsRadius, int bombsDelay, int actionsPerTurn, int invulnerableTurns) : id(id), x(0), y(0), health(health), bombsCount(bombsCount), bombsDelay(bombsDelay), bombsRadius(bombsRadius), actionsPerTurn(actionsPerTurn), canTriggerBombs(false), invulnerableTurns(0) {}
    int getId() const;
    int getX() const;
    int getY() const;
    int getHealth() const;
    int getBombsCount() const;
    int getBombsRadius() const;
    int getBombsDelay() const;
    int getActionsPerTurn() const;
    bool getCanTriggerBomb() const;
    int getInvulnerableTurns() const;
    void setX(int x);
    void setY(int y);
    void setHealth(int health);
    void setBombsCount(int bombs);
    void setBombsRadius(int bombsRadius);
    void setBombsDelay(int bombsDelay);
    void setActionsPerTurn(int actionsPerTurn);
    void setCanTriggerBombs(bool canTriggerBombs);
    void setInvulnerable(int invulnerableTurns);

};


#endif

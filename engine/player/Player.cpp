#include "Player.h"

int Player::getId() const {
    return id;
}

int Player::getX() const {
    return x;
}

void Player::setX(int x) {
    this->x = x;
}

int Player::getY() const {
    return y;
}

bool Player::getCanTriggerBomb() const {
    return canTriggerBombs;
}

void Player::setY(int y) {
    this->y = y;
}

int Player::getHealth() const {
    return health;
}

void Player::setHealth(int health) {
    this->health = health;
}

int Player::getBombsCount() const {
    return bombsCount;
}

void Player::setBombsCount(int bombsCount) {
    this->bombsCount = bombsCount;
}

int Player::getBombsRadius() const {
    return bombsRadius;
}

void Player::setBombsRadius(int bombsRadius) {
    this->bombsRadius = bombsRadius;
}

int Player::getBombsDelay() const {
    return bombsDelay;
}

void Player::setBombsDelay(int bombsDelay) {
    this->bombsDelay = bombsDelay;
}

int Player::getActionsPerTurn() const {
    return actionsPerTurn;
}

void Player::setActionsPerTurn(int actionsPerTurn) {
    this->actionsPerTurn = actionsPerTurn;
}

void Player::setCanTriggerBombs(bool canTriggerBombs) {
    this->canTriggerBombs = canTriggerBombs;
}

void Player::setInvulnerable(int invulnerableTurns) {
    this->invulnerableTurns = invulnerableTurns;
}

int Player::getInvulnerableTurns() const {
    return this->invulnerableTurns;
}

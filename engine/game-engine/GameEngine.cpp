#include "GameEngine.h"
#include <algorithm>
#include <string>
#include <iostream>
#include <iomanip>
#include <ctime>
#include <utility>
#include <tuple>
#include <random>
#include <sstream>
#include <fstream>
#include <sys/stat.h>


static const char GRID_EMPTY = '_';
static const char GRID_WALL = '#';
static const char GRID_BREAKABLE_WALL = '%';
static const char GRID_BOMB = 'o';
static const char GRID_HORIZONTAL_EXPLOSION = '-';
static const char GRID_VERTICAL_EXPLOSION = '|';
static const int MIN_BOMB_RADIUS = 1;
static const int MIN_BOMB_DELAY = 2;
static const int MIN_BOMB_COUNT = 1;
static const int MIN_HEALTH = 1;
static const int MIN_ACTIONS_COUNT = 1;
static const std::string ACTION_UP = "U";
static const std::string ACTION_DOWN = "D";
static const std::string ACTION_LEFT = "L";
static const std::string ACTION_RIGHT = "R";
static const std::string ACTION_BOMB = "B";
static const std::string ACTION_TRIGGER = "T";
static const std::string ACTION_NO_ACTION = "NOACTION";
static const std::vector<char> AVAILABLE_ITEMS = {'r', 'R', 'R', 'R', 'R', 'd', 'D', 'D', 'n', 'N', 'N', 'a', 'A', 'A', 'H', 'H', 'T', 'G'};


GameEngine::GameEngine(int gridWidth, int gridHeight, int bombsCount, int bombsDelay, int bombsRadius, bool randomWalls, bool gridRing, int deadline, bool immortalPlayers, int itemsSpawnRate, int breakableWallsRate, int wallsDensity, int baseHealth) : gridWidth(gridWidth), gridHeight(gridHeight), bombsCount(bombsCount), bombsDelay(bombsDelay), bombsRadius(bombsRadius), randomWalls(randomWalls), gridRing(gridRing), deadline(deadline), immortalPlayers(immortalPlayers), itemsSpawnRate(itemsSpawnRate), breakableWallsRate(breakableWallsRate), wallsDensity(wallsDensity), baseHealth(baseHealth) {
    this->init();
}

void GameEngine::init() {
    this->initReplayFile();
    this->initPlayers();
    this->sendSettings();
    this->generateGrid();
    this->placePlayers();
}

void GameEngine::initReplayFile() {
    std::string replaysDirName = "replays";
    mkdir(replaysDirName.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    std::ostringstream oss;
    oss << std::put_time(&tm, "%d-%m-%Y %H-%M-%S");
    this->replayFilePath = replaysDirName + "/" + "Replay " + oss.str() + ".txt";

    std::ofstream outfile(this->replayFilePath);
    outfile.close();
}

std::vector<Player*> GameEngine::getPlayers() const {
    return this->players;
}

void GameEngine::generateGrid() {
    std::random_device rd;
    std::default_random_engine engine(rd());
    std::uniform_int_distribution<int> dist(0, 100);

    std::vector<std::string> grid;

    for (int i = 0; i < this->gridHeight; i++) {
        std::string line;

        for (int j = 0; j < this->gridWidth; j++) {
            char ch = GRID_EMPTY;

            if (j == 0 || j == this->gridWidth - 1 || i == 0 || i == this->gridHeight - 1) {
                if (this->gridRing) {
                    int random = dist(engine);
                    if (random < this->wallsDensity) {
                        ch = random < this->breakableWallsRate ? GRID_BREAKABLE_WALL : GRID_WALL;
                    }
                } else {
                    ch = GRID_WALL;
                }
            } else {
                int random = dist(engine);
                if (random < this->wallsDensity) {
                    ch = random < this->breakableWallsRate ? GRID_BREAKABLE_WALL : GRID_WALL;
                }
            }

            line += ch;
        }

        grid.push_back(line);
    }

    this->grid = grid;
}

void GameEngine::placePlayers() {
    for (auto &player: players) {
        this->placePlayer(player);
    }
}

void GameEngine::placeItem(Item* item) {
    while (true) {
        std::random_device rd;
        std::default_random_engine engine(rd());
        std::uniform_int_distribution<int> uniform_distX(0, this->gridWidth - 1);
        std::uniform_int_distribution<int> uniform_distY(0, this->gridHeight - 1);
        int x = uniform_distX(engine);
        int y = uniform_distY(engine);

        if (this->grid[y][x] == GRID_EMPTY) {
            item->setX(x);
            item->setY(y);
            break;
        }
    }
}

void GameEngine::placePlayer(Player* player) {
    while (true) {
        std::random_device rd;
        std::default_random_engine engine(rd());
        std::uniform_int_distribution<int> uniform_distX(0, this->gridWidth - 1);
        std::uniform_int_distribution<int> uniform_distY(0, this->gridHeight - 1);
        int x = uniform_distX(engine);
        int y = uniform_distY(engine);

        if (this->grid[y][x] == GRID_EMPTY) {
            player->setX(x);
            player->setY(y);
            break;
        }
    }
}

void expectMessage(const std::string& message) {
    std::string input;
    std::getline(std::cin, input);

    if (input != message) {
        std::cout << "Expected input was '" + message + "', '" + input + "' received";
        throw std::exception();
    }
}

void GameEngine::receivePlayers() {
    std::string input;
    std::getline(std::cin, input);
    int playersCount = std::stoi(input);

    if (std::cin.fail()) {
        throw std::exception();
    }

    this->playersLeft = playersCount;

    for (int id = 1; id < playersCount + 1; id++) {
        auto * player = new Player(id, baseHealth, bombsCount, bombsRadius, bombsDelay, actionsPerTurn, 0);
        this->players.push_back(player);
    }
}

void GameEngine::sendSettings() {
    std::cout << "START settings" << std::endl;
    std::cout << "NB_BOMBS " << this->bombsCount << std::endl;
    std::cout << "BOMB_DURATION " << this->bombsDelay << std::endl;
    std::cout << "BOMB_RADIUS " << this->bombsRadius << std::endl;
    std::cout << "STOP settings" << std::endl;
}

void GameEngine::initPlayers() {
    expectMessage("START players");
    this->receivePlayers();
    expectMessage("STOP players");
}

void GameEngine::sendGridInformation() const {
    std::cout << this->gridWidth << " " << this->gridHeight << std::endl;

    for (int line = 0; line < this->gridHeight; line++) {
        std::cout << this->grid[line] << std::endl;
    }
}

void GameEngine::applyPlayerAction(Player *player, std::string action) {
    int x = player->getX();
    int y = player->getY();

    if (player->getHealth() == 0) {
        return;
    } else if (action == ACTION_NO_ACTION) {
        return;
    } else if (action == ACTION_UP) {
        y--;
    } else if (action == ACTION_DOWN) {
        y++;
    } else if (action == ACTION_LEFT) {
        x--;
    } else if (action == ACTION_RIGHT) {
        x++;
    } else if (action == ACTION_BOMB && player->getBombsCount() > 0) {
        this->bombs.push_back(new Bomb(player, x, y, player->getBombsDelay(), player->getBombsRadius()));
        player->setBombsCount(player->getBombsCount() - 1);
    } else if (action == ACTION_TRIGGER && player->getCanTriggerBomb()) {
        this->triggerPlayerBombs(player);
    }

    this->movePlayer(player, y, x);
}

void GameEngine::movePlayer(Player* player, int y, int x) {
    if (this->gridRing) {
        x = x < 0 ? this->gridWidth - 1 : x > this->gridWidth - 1 ? 0 : x;
        y = y < 0 ? this->gridHeight - 1 : y > this->gridHeight - 1 ? 0 : y;
    }

    if (this->grid[y][x] != GRID_WALL && this->grid[y][x] != GRID_BREAKABLE_WALL && this->grid[y][x] != '1' && this->grid[y][x] != '2' && this->grid[y][x] != '3' && this->grid[y][x] != '4' && this->grid[y][x] != '5' && this->grid[y][x] != '6' && this->grid[y][x] != '7' && this->grid[y][x] != '8' && this->grid[y][x] != '9') {
        player->setY(y);
        player->setX(x);
    }
}

void GameEngine::triggerPlayerBombs(Player* player) {
    for (auto &bomb: this->bombs) {
        if (bomb->getOwner()->getId() == player->getId()) {
            bomb->setDelay(0);
        }
    }
}

void GameEngine::getPlayerActions(const int& turn, Player* player) {
    std::string inputStart = "START actions " + std::to_string(turn) + " " + std::to_string(player->getId());
    std::string inputStop = "STOP actions " + std::to_string(turn) + " " + std::to_string(player->getId());
    expectMessage(inputStart);

    std::ofstream outfile(this->replayFilePath, std::ios_base::app);
    outfile << inputStart << std::endl;

    std::string action;
    do {
        std::getline(std::cin, action);
        outfile << action << std::endl;
        this->applyPlayerAction(player, action);
    } while(action != inputStop);

    outfile.close();
}

int GameEngine::getDeadline() const {
    return deadline;
}

Player* GameEngine::getWinner() const {
    Player *winner = nullptr;

    for (auto &player: this->players) {
        if (player->getHealth() > 0) {
            winner = player;
        }
    }

    return winner;
}

void GameEngine::sendWinner() {
    if (this->playersLeft == 0) {
        std::cout << "WINNER 0" << std::endl;
    } else {
        Player* winner = this->getWinner();
        std::cout << "WINNER " << std::to_string(winner->getId()) << std::endl;
    }
}

void GameEngine::playerTurn(int turn, Player* player) {
    std::cout << "START turn " << turn << " " << player->getId() << std::endl;

    if (this->playersLeft <= 1) {
        this->sendWinner();
    } else if (this->playersLeft > 1 && this->deadline > 0 && this->deadline == turn) {
        std::cout << "WINNER 0" << std::endl;
    } else {
        this->sendGridInformation();
    }

    std::cout << "STOP turn " << turn << " " << player->getId() << std::endl;
    this->getPlayerActions(turn, player);
}

void GameEngine::clearGrid() {
    for (int i = 0; i < this->gridHeight; i++) {
        for (int j = 0; j < this->gridWidth; j++) {
            if (this->grid[i][j] != GRID_WALL && this->grid[i][j] != GRID_BREAKABLE_WALL) {
                this->grid[i][j] = GRID_EMPTY;
            }
        }
    }
}

void GameEngine::displayBombs() {
    for (auto &bomb : this->bombs) {
        this->grid[bomb->getY()][bomb->getX()] = GRID_BOMB;
    }
}

void GameEngine::displayItems() {
    this->spawnRandomItem();

    for (auto &item : this->items) {
        this->grid[item->getY()][item->getX()] = item->getSymbol();
    }
}

void GameEngine::useItem(Player* player, Item* item) {
    player->setBombsDelay(std::max(player->getBombsDelay() + item->getBombsDelayModifier(), MIN_BOMB_DELAY));
    player->setBombsCount(std::max(player->getBombsCount() + item->getBombsCountModifier(), MIN_BOMB_COUNT));
    player->setBombsRadius(std::max(player->getBombsRadius() + item->getBombsRadiusModifier(), MIN_BOMB_RADIUS));
    player->setActionsPerTurn(std::max(player->getActionsPerTurn() + item->getActionsCountModifier(), MIN_ACTIONS_COUNT));
    player->setHealth(std::max(player->getHealth() + item->getPlayerHealthModifier(), MIN_HEALTH));
    player->setCanTriggerBombs(player->getCanTriggerBomb() || item->isCanTriggerBombsModifier());
    player->setInvulnerable(item->getPlayerInvulnerableModifier());

    auto position = std::find(this->items.begin(), this->items.end(), item);
    if (position != this->items.end()) {
        this->items.erase(position);
    }
}

void GameEngine::pickUpItem(Player *player) {
    for (auto &item : this->items) {
        if (item->getY() == player->getY() && item->getX() == player->getX()) {
            this->useItem(player, item);
        }
    }
}

void GameEngine::displayPlayers() {
    for (auto &player : this->players) {
        if (player->getHealth() > 0) {
            this->grid[player->getY()][player->getX()] = std::to_string(player->getId())[0];
            this->pickUpItem(player);
        }
    }
}

void GameEngine::triggerBombAtPosition(int y, int x) {
    for (auto &bomb : this->bombs) {
        if (bomb->getX() == x && bomb->getY() == y) {
            this->triggerBomb(bomb);
        }
    }
}

int GameEngine::getPlayersLeft() const {
    return this->playersLeft;
}

void GameEngine::hitPlayerAtPosition(int y, int x) {
    for (auto &player : this->players) {
        if (player->getX() == x && player->getY() == y && player->getHealth() > 0 && player->getInvulnerableTurns() == 0) {
            player->setHealth(std::max(player->getHealth() - 1, 0));

            if (player->getHealth() == 0) {
                this->playersLeft--;
                std::cerr << "bomb kills player " <<  player->getId() << " at (x: " << x << ", y: " << y << ")" << std::endl;
            }
        }
    }
}

void GameEngine::triggerBomb(Bomb* bomb) {
    bomb->getOwner()->setBombsCount(bomb->getOwner()->getBombsCount() + 1);
    auto position = std::find(this->bombs.begin(), this->bombs.end(), bomb);
    if (position != this->bombs.end()) {
        this->bombs.erase(position);
    }

    int x = bomb->getX();
    int y = bomb->getY();

    std::vector<std::tuple<int, int>> directions;
    directions.emplace_back(std::make_tuple(1, 0));
    directions.emplace_back(std::make_tuple(-1, 0));
    directions.emplace_back(std::make_tuple(0, 1));
    directions.emplace_back(std::make_tuple(0, -1));

    for (auto &direction : directions) {
        for (int i = 0; i < bomb->getRadius() + 1; i++ ) {
            int cx = x + i * std::get<0>(direction);
            int cy = y + i * std::get<1>(direction);

            if (cy < 0 || cy > this->gridHeight - 1 || cx < 0 || cx > this->gridWidth - 1) {
                break;
            }

            if (this->grid[cy][cx] == GRID_WALL) {
                break;
            }

            if (this->grid[cy][cx] == GRID_BREAKABLE_WALL) {
                this->grid[cy][cx] = x != cx ? GRID_HORIZONTAL_EXPLOSION : y != cy ? GRID_VERTICAL_EXPLOSION : this->grid[cy][cx];
                break;
            }

            this->grid[cy][cx] = x != cx ? GRID_HORIZONTAL_EXPLOSION : y != cy ? GRID_VERTICAL_EXPLOSION : this->grid[cy][cx];
            this->triggerBombAtPosition(cy, cx);

            if (!this->immortalPlayers) {
                this->hitPlayerAtPosition(cy, cx);
            }
        }
    }
}

void GameEngine::updateBombs() {
    for (auto &bomb : this->bombs) {
        if (bomb->getDelay() > 0 && !bomb->getOwner()->getCanTriggerBomb()) {
            bomb->setDelay(bomb->getDelay() - 1);
        } else if (bomb->getDelay() == 0) {
            this->triggerBomb(bomb);
        }
    }
}

Item* GameEngine::getRandomItem() {
    Item *item = nullptr;
    std::random_device rd;
    std::default_random_engine engine(rd());
    std::uniform_int_distribution<long> dist(0, AVAILABLE_ITEMS.size() - 1);
    long random = dist(engine);
    char symbol = AVAILABLE_ITEMS[random];

    switch(symbol) {
        case 'r':
            item = new Item(symbol, -1, 0, 0, 0, 0, false, 0);
            break;
        case 'R':
            item = new Item(symbol, 1, 0, 0, 0, 0, false, 0);
            break;
        case 'd':
            item = new Item(symbol, 0, -1, 0, 0, 0, false, 0);
            break;
        case 'D':
            item = new Item(symbol, 0, 1, 0, 0, 0, false, 0);
            break;
        case 'n':
            item = new Item(symbol, 0, 0, -1, 0, 0, false, 0);
            break;
        case 'N':
            item = new Item(symbol, 0, 0, 1, 0, 0, false, 0);
            break;
        case 'a':
            item = new Item(symbol, 0, 0, 0, -1, 0, false, 0);
            break;
        case 'A':
            item = new Item(symbol, 0, 0, 0, 1, 0, false, 0);
            break;
        case 'T':
            item = new Item(symbol, 0, 0, 0, 0, 0, true, 0);
            break;
        case 'H':
            item = new Item(symbol, 0, 0, 0, 0, 1, false, 0);
            break;
        case 'G':
            item = new Item(symbol, 0, 0, 0, 0, 0, false, 10);
            break;
        default:
            break;
    }

    this->placeItem(item);
    return item;
}

void GameEngine::spawnRandomItem() {
    std::random_device rd;
    std::default_random_engine engine(rd());
    std::uniform_int_distribution<int> dist(0, 100);
    int random = dist(engine);

    if (random < this->itemsSpawnRate) {
        Item* item = this->getRandomItem();
        this->items.push_back(item);
    }
}

void GameEngine::updateItemsEffects() {
    for (auto &player : this->players) {
        if (player->getInvulnerableTurns() > 0) {
            player->setInvulnerable(std::max(player->getInvulnerableTurns() - 1, 0));
        }
    }
}

void GameEngine::update() {
    this->clearGrid();
    this->displayBombs();
    this->displayItems();
    this->displayPlayers();
    this->updateBombs();
    this->updateItemsEffects();
}

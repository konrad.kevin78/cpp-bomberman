#ifndef ENGINE_GAMEENGINE_H
#define ENGINE_GAMEENGINE_H


#include <string>
#include <vector>
#include "../player/Player.h"
#include "../bomb/Bomb.h"
#include "../item/Item.h"

class GameEngine {
private:
    int gridWidth;
    int gridHeight;
    int bombsCount;
    int bombsDelay;
    int bombsRadius;
    int actionsPerTurn = 1;
    int playersLeft;
    bool randomWalls;
    bool gridRing;
    int deadline;
    bool immortalPlayers;
    int itemsSpawnRate;
    int breakableWallsRate;
    int wallsDensity;
    int baseHealth;
    std::string replayFilePath;
    std::vector<std::string> grid;
    std::vector<Player*> players;
    std::vector<Bomb*> bombs;
    std::vector<Item*> items;

    void init();
    void initPlayers();
    void sendSettings();
    void generateGrid();
    void placePlayers();
    void receivePlayers();
    void placePlayer(Player* playerId);
    void sendGridInformation() const;
    void getPlayerActions(const int& turn, Player* player);
    void applyPlayerAction(Player *player, std::string action);
    void clearGrid();
    void displayBombs();
    void displayItems();
    void displayPlayers();
    void updateBombs();
    void triggerBomb(Bomb* bomb);
    void triggerBombAtPosition(int x, int y);
    void triggerPlayerBombs(Player* player);
    void hitPlayerAtPosition(int x, int y);
    void sendWinner();
    void spawnRandomItem();
    Player* getWinner() const;
    void pickUpItem(Player* player);
    void useItem(Player* player, Item* item);
    void placeItem(Item* item);
    void movePlayer(Player* player, int y, int x);
    Item* getRandomItem();
    void updateItemsEffects();
    void initReplayFile();

public:
    GameEngine(int gridWidth, int gridHeight, int bombsCount, int bombsDelay, int bombsRadius, bool randomWalls, bool gridRing, int deadline, bool immortalPlayers, int itemsSpawnRate, int breakableWallsRate, int wallsDensity, int baseHealth);
    int getPlayersLeft() const;
    int getDeadline() const;
    std::vector<Player*> getPlayers() const;
    void playerTurn(int turn, Player* player);
    void update();
};


#endif

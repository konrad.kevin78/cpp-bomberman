#include <QDesktopWidget>
#include <QGridLayout>
#include <QLabel>
#include <QListWidget>
#include <QListWidgetItem>
#include <QHBoxLayout>
#include <QScrollArea>
#include <QRect>
#include "mainview.h"


MainView::MainView(QWidget *parent) : QWidget(parent) {

    QDesktopWidget dw;
    this->setFixedSize(dw.width(), dw.height());
    this->setStyleSheet("background-color:black;");

    auto * scrollArea = new QScrollArea(this);
    scrollArea->setGeometry(this->width()/14,this->height() / 12,this->width()/2 ,this->height() / 1.5);
    scrollArea->setWidgetResizable(true);
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    auto *HBoxWgt = new QWidget();
    auto *hBox = new QHBoxLayout();
    HBoxWgt->setLayout(hBox);
    HBoxWgt->setLayoutDirection(Qt::RightToLeft);
    scrollArea->setWidget(HBoxWgt);

    for (int l = 0; l < 20; l++) {
        int j = 0;
        int i = 0;
        auto *gridWidget = new QWidget();
        auto *layout = new QGridLayout();
        gridWidget->setLayout(layout);

        for (i = 0; i < 20; i++) {
            for (j= 0; j < 20; j++) {
                QLabel* test = new QLabel();
                test->setText("_");

                if (i == 0 || i == (20 - 1) || j == 0 || j == (20 - 1)) {
                    test->setText("#");
                }

                test->setStyleSheet("QLabel{ color : white; font-style: bold}");
                layout->addWidget(test,i,j);
            }
        }

        QLabel* title = new QLabel(QString::number((l)));
        title->setStyleSheet("QLabel{ color : white; font-style: bold}");
        title->setAlignment(Qt::AlignCenter);
        layout->addWidget(title,i+1,0,2,j);

        hBox->addWidget(gridWidget);
        hBox->setSpacing(300);
    }
}
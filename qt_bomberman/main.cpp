#include <iostream>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>
#include "mainView.h"
#include "Console.h"

int main(int argc, char *argv[]) {

    QApplication app(argc, argv);

    MainView v;
    v.show();

    Console console;
    console.run();
    QObject::connect(&console, SIGNAL(quit()), &app, SLOT(quit()));
    return app.exec();
}
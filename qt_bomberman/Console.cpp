#include <iostream>
#include "Console.h"

int step = 0;
int turn = 1;

void Console::run() {
    connect(m_notifier, SIGNAL(activated(int)), this, SLOT(readCommand()));
}

void expectMessage(const std::string& message) {
    std::string input;
    std::getline(std::cin, input);

    if (input != message) {
        std::cout << "Expected input was '" + message + "', '" + input + "' received";
        throw std::exception();
    }
}

void Console::readCommand() {
    std::string line;
    std::getline(std::cin, line);

    if (step == 0 && line == "START player") {
        step++;
    }

    if (step == 1) {
        int inputId = std::stoi(line);

        if (std::cin.fail()) {
            throw std::exception();
        }

        this->id = inputId;
        step++;
    }

    if (step == 2 && line == "STOP player") {
        step++;
    }

    if (step == 3 && line == "START settings") {
        step++;
    }

    if (step == 4) {
        // TODO : get settings
    }

    if (step == 4 && line == "STOP settings") {
        step++;
    }
}